class Mutations::AddUser < GraphQL::Schema::Mutation
  null false

  argument :name,     String, required: true
  argument :age,      Int,    required: false
  argument :team_id,  ID,     required: false
  argument :score,    Int,    required: false

  field :user,     Types::UserType,  null: false
  field :errors,   [String],         null: false

  def resolve(name: "noname", age: rand(30), team_id: rand(10), score: rand(100))
    user = User.new(name: name, age: age, team_id: team_id, score: score)
    if user.save
      { user: user, errors: [] }
    else
      { user: user, errors: user.errors.full_messages }
    end
  end
end
