class Mutations::DeleteUser < GraphQL::Schema::Mutation
  null false

  argument :id,      ID,     required: true

  # 公式のサンプルでは、mutationのresponseは { object, errors } を返していました
  # トップレベルのerror
  field :user,     Types::UserType,  null: false
  field :errors,   [String],         null: false

  def resolve(id:)
    user = User.find(id)

    if user.destroy
      { user: user, errors: [] }
    else
      {
        user: user,
        errors: user.errors.full_messages
      }
    end
  end
end
