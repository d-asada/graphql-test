module Types
  class MutationType < Types::BaseObject
    field :delete_user, null: false, description: "ユーザ削除", mutation: Mutations::DeleteUser
    field :add_user, null: false, description: "ユーザ追加", mutation: Mutations::AddUser
  end
end
