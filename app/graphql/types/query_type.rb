module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    # field :shobon, String, null: false, description: "かなしみ"
    field :users, Types::UserType.connection_type, null: false, description: "ユーザ一覧"
    field :teams, Types::TeamType.connection_type, null: false, description: "チーム一覧"
    field :team, Types::TeamType, null: true do
      description "指定チーム取得"
      argument :id, ID, 'チームのID', required: true
    end
    field :user, Types::UserType, null: true do
      description "指定ユーザ取得"
      argument :id, ID, 'ユーザのID', required: true
    end

    def teams
      Team.all
    end

    def users
      User.all
    end

    def user(id:)
      User.find(id)
    end

    def team(id:)
      Team.find(id)
    end

    def shobon
      "(´・ω・｀)"
    end
  end
end
