class Types::UserType < GraphQL::Schema::Object
  field :id,        ID,              null: false
  field :name,      String,          null: false
  field :age,       Int,             null: false
  field :score,     Int,             null: false
  field :team,      Types::TeamType, null: true do
    description 'ユーザのチーム'
  end

  def team
    Loaders::RecordLoader.for(User).load(object.team_id)
  end
end
