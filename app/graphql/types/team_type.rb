class Types::TeamType < GraphQL::Schema::Object
  field :id,        ID,                              null: false
  field :name,      String,                          null: false
  field :users,     Types::UserType.connection_type, null: true do
    description 'ユーザ一覧をpaginationで取得'
  end

  def users
    Loaders::AssociationLoader.for(Team, :users).load(object)
  end
end
